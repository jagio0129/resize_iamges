# Description
画像をリサイズするツールです。

縦か横の小さいほうが300pxにリサイズされ、もう一方は比率が変わらないようにリサイズされます。

e.g.
  縦:1200, 横:900 => 縦:400, 横:300
  縦:900, 横:1200 => 縦:300, 横:400

## env
- Ubuntu 18.04
- ruby 2.7.0
- rmagick 4.0.0

## Setup

```
sudo apt-get install libmagickwand-dev
bundle install
```

## Usage

1. `./img/origin/`にリサイズしたい画像を配置
2. `bundle exec ruby main.rb`
3. `./img/resized/`にリサイズ(縦か横が300)された画像ができる
