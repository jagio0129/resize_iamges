require 'rmagick'
require 'open3'

ORIGIN_IMG_PATH = "./img/origin/"
RESIZED_IMG_PATH = "./img/resized/"

def origin_images
  o, e, s = Open3.capture3("ls #{ORIGIN_IMG_PATH}")

  raise 'error' unless e.empty?
  raise 'empty data' if o.empty?
  o.split("\n")
end

# どちらか小さいほうのサイズでリサイズされる
# 参考) http://blog.ruedap.com/2011/03/22/ruby-rmagick-imagemagick-resize-crop
def resize(image)
  new_filename = RESIZED_IMG_PATH + rename(image)

  img = Magick::ImageList.new(ORIGIN_IMG_PATH + image)
  new_img = img.resize_to_fit(300, 300)
  new_img.write(new_filename)
  p "resiezed to #{new_filename}"
end

def rename(fiilename)
  basename = File.basename(fiilename, '.*')
  extname  = File.extname(fiilename)
  basename + '_resized' + extname
end

if __FILE__ == $0
  imgs = origin_images
  imgs.each do |f|
    resize(f)
  end
end
